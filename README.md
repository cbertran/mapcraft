![logo](https://gitlab.com/cbertran/mapcraft/-/raw/main/src/dist/img/icon/icon_small.png "Logo")

---

[![Website](https://img.shields.io/website?style=flat-square&url=https%3A%2F%2Fmapcraft.app)](https://mapcraft.app)


## Mapcraft is a software to increase the native possibilities of Minecraft Java edition without any mod installation

### [Go to the official website](https://mapcraft.app/)

## Setup
1. Download the latest stable version [here](https://gitlab.com/cbertran/mapcraft/-/releases)
2. Install it in the folder of your choice
3. Now you just have to launch the executable.

##### <span>&#x26a0;</span> If you wish, it is possible to have access to the Mapcraft editing tools just by installing the [datapack](https://gitlab.com/cbertran/mapcraft-datapack/) and [resourcepack](https://gitlab.com/cbertran/mapcraft-resourcepack/) manually, but the other components will not be usable

## Developer(s)
- Clément Bertrand [(@cbertran)](https://gitlab.com/cbertran)

## License
    Mapcraft Copyright (C) 2020 - ...  Clément Bertrand
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
